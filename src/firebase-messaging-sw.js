importScripts('https://notifications-ruby-beta.vercel.app/firebasejs/10.7.1/firebase-app-compat.js');
importScripts('https://notifications-ruby-beta.vercel.app/firebasejs/10.7.1/firebase-messaging-compat.js');
importScripts('https://notifications-ruby-beta.vercel.app/firebasejs/10.7.1/firebase-analytics-compat.js')

firebase.initializeApp(
    {
        apiKey: "AIzaSyCpkykmx9WcxZwOpysc-Y3CH_pUKkc_pGA",
        authDomain: "notifications-20a28.firebaseapp.com",
        projectId: "notifications-20a28",
        storageBucket: "notifications-20a28.appspot.com",
        messagingSenderId: "470381838572",
        appId: "1:470381838572:web:d0cde6a765113d2abf25c9",
        measurementId: "G-8DT3T9FL20"
    }
)

const messaging = firebase.messaging()