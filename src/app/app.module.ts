import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AngularFireModule } from '@angular/fire/compat';
import { AngularFireMessagingModule } from '@angular/fire/compat/messaging';
import { environments } from 'src/environments/environments';
import { PushNotificationsService } from './services/push-notifications.service';
import { AsyncPipe } from '@angular/common';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environments.firebase),
    AngularFireMessagingModule
  ],
  providers: [PushNotificationsService, AsyncPipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
