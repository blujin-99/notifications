import { Component } from '@angular/core';
import { PushNotificationsService } from './services/push-notifications.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'notifications';
  constructor(private notification: PushNotificationsService) {

  }

  ngOnInit(): void {
    this.notification.requestPermission()
  }

}
