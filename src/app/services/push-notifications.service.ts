import { Injectable } from '@angular/core';
import { AngularFireMessaging } from '@angular/fire/compat/messaging';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PushNotificationsService {

  currentMessaging = new BehaviorSubject(null)
  
  constructor(private AFmessaging: AngularFireMessaging) { }

  requestPermission(){
    this.AFmessaging.requestToken.subscribe(
      (token)=> {
        console.log(token)
      },
      (error) => {
        console.log(error + 'No se pudo obtener el token')
      }
    )
  }
  

}
